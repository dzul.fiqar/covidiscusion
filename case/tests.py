from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse
from selenium import webdriver
from .models import Kasus
from .forms import FormKasus
from .views import case, detail

class MainTestCase(TestCase):
    def setUp(self):
        Kasus.objects.create(jumlah=2, prov="DKI Jakarta", kotkab="Jakarta Pusat")
        Kasus.objects.create(jumlah=1, prov="DKI Jakarta", kotkab="jakARta pusaT")
        Kasus.objects.create(jumlah=1, prov="DKI Jakarta", kotkab="  jaKarta     puSat   ")
        Kasus.objects.create(jumlah=1, prov="DKI Jakarta", kotkab="jakarta pusat ")
        Kasus.objects.create(jumlah=1, prov="DKI Jakarta", kotkab="jakarta pusat")
        Kasus.objects.create(jumlah=15, prov="DIY", kotkab="Yogyakarta")

    def test_post_case(self):
        response = self.client.post("/case/", data={"jumlah": "2", "prov": "DKI Jakarta", "kotkab": "Jakarta Pusat"})
        self.assertEqual(response.status_code, 302)

    def test_model_buat(self):
        self.assertEqual(Kasus.objects.all().count(), 6)
        self.assertEqual(Kasus.objects.get(id=1).prov, "DKI Jakarta")
        self.assertEqual(Kasus.objects.get(id=1).jumlah, 2)
        self.assertEqual(Kasus.objects.get(id=1).kotkab, "Jakarta Pusat")

    def test_cari_kota(self):
        response = Client().get('/case/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Jakarta Pusat", html_kembalian)
        self.assertIn("Yogyakarta", html_kembalian)
        self.assertNotIn("jakARta pusaT", html_kembalian)
        self.assertNotIn("  jaKarta     puSat   ", html_kembalian)
        self.assertNotIn("jakarta pusat ", html_kembalian)
        self.assertNotIn("jakarta pusat", html_kembalian)
        self.assertIn("DKI Jakarta : 6", html_kembalian)
        self.assertIn("DIY : 15", html_kembalian)

    def test_detail_cari_kota(self):
        response = Client().get('/case/detail/1')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Jakarta Pusat", html_kembalian)
        self.assertIn("DKI Jakarta", html_kembalian)
        self.assertIn("6 Kasus", html_kembalian)
        self.assertNotIn("jakARta pusaT", html_kembalian)
        self.assertNotIn("  jaKarta     puSat   ", html_kembalian)
        self.assertNotIn("jakarta pusat ", html_kembalian)
        self.assertNotIn("jakarta pusat", html_kembalian)

    

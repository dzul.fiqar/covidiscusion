from django.shortcuts import render, redirect
from .forms import FormExperience
from .models import Experience

# Create your views here.
def experiences(request):
    return render(request, 'experiences/experiences.html', {'form' : FormExperience, 'experiences' : Experience.objects.all()})

def saveexperience(request): 
    experience = FormExperience(request.POST or None)
    if experience.is_valid() and request.method == 'POST':
        experience.save()
    
    return redirect('experiences:experiences')

def deleteexperience(request, pk):
    experience = Experience.objects.get(id=pk)
    if request.method == 'POST':
        experience.delete()

    return redirect('experiences:experiences')
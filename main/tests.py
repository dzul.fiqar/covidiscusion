from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from .models import Saran
from .forms import FormSaran
from .views import home, savesaran, deletesaran

class MainTestCase(TestCase):
    def test_url_home(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_template_home(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'base.html')

    def setUp(self):
        saran = Saran.objects.create(nama="Sakura", lokasi="Depok", saran="test1")

    def test_views_use_saran(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_save_saran(self):
        response = self.client.post('/savesaran', data={'nama':'Sakura','lokasi':'Depok','saran':'test2'})
        self.assertEqual(Saran.objects.all().count(), 2)
        self.assertEqual(Saran.objects.get(id=2).nama, "Sakura")
        self.assertEqual(Saran.objects.get(id=2).lokasi, "Depok")
        self.assertEqual(Saran.objects.get(id=2).saran, "test2")

    def test_delete_saran(self):
        response = self.client.post('/deletesaran/1')
        self.assertEqual(Saran.objects.all().count(), 0)
from django.shortcuts import render, redirect
from .models import Saran
from .forms import FormSaran
def home(request):
    return render(request, 'main/home.html', {'form':FormSaran, 'saran':Saran.objects.all()})

def savesaran(request):
	saran = FormSaran(request.POST or None)
	if saran.is_valid() and request.method == 'POST':
		saran.save()
	return redirect('main:home')

def deletesaran(request, pk):
	saran = Saran.objects.get(id=pk)
	if request.method == 'POST':
		saran.delete()
	return redirect('main:home')